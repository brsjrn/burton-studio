-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 06 Novembre 2017 à 23:30
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `burton_studio`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adress_line_1` varchar(255) DEFAULT NULL,
  `adress_line_2` varchar(255) DEFAULT NULL,
  `adress_country` varchar(31) DEFAULT NULL,
  `phone` varchar(31) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `adress_line_1`, `adress_line_2`, `adress_country`, `phone`, `email`) VALUES
(1, '307 S Cedros Ave,', 'Solana Beacj, CA 92075', 'USA', '+1 858 794 7204', 'mon@email.com');

-- --------------------------------------------------------

--
-- Structure de la table `modimage`
--

CREATE TABLE IF NOT EXISTS `modimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `modimage`
--

INSERT INTO `modimage` (`id`, `image`, `ref_project`, `order_elt`) VALUES
(23, 'Modimage_image 58bfe4c8d62fe.jpg', 18, 5),
(30, 'Modimage_image 58e2db21381fb.jpg', 23, 2),
(31, 'Modimage_image 5940500539131.jpg', 20, 1),
(32, 'Modimage_image 5940501ebb506.jpg', 20, 5);

-- --------------------------------------------------------

--
-- Structure de la table `modimg`
--

CREATE TABLE IF NOT EXISTS `modimg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `modlargetitle`
--

CREATE TABLE IF NOT EXISTS `modlargetitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `modslider`
--

CREATE TABLE IF NOT EXISTS `modslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `modslider`
--

INSERT INTO `modslider` (`id`, `ref_project`, `order_elt`) VALUES
(5, 18, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modspace`
--

CREATE TABLE IF NOT EXISTS `modspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `modtext`
--

CREATE TABLE IF NOT EXISTS `modtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  KEY `ref_project_2` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `modtext`
--

INSERT INTO `modtext` (`id`, `text`, `ref_project`, `order_elt`) VALUES
(16, 'Musheireb is located in the heart of Doha. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.', 18, 2),
(19, 'qsdqsdaaazzzz', 18, 4),
(20, 'qsdq', 20, 3);

-- --------------------------------------------------------

--
-- Structure de la table `modtitle`
--

CREATE TABLE IF NOT EXISTS `modtitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `modtitle`
--

INSERT INTO `modtitle` (`id`, `title`, `ref_project`, `order_elt`) VALUES
(12, 'qsdqsd', 20, 4),
(13, 'sdf', 20, 6),
(15, 'Title', 19, 3);

-- --------------------------------------------------------

--
-- Structure de la table `modvideo`
--

CREATE TABLE IF NOT EXISTS `modvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `embeded_code` varchar(255) DEFAULT NULL,
  `image_cover` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `modvideo`
--

INSERT INTO `modvideo` (`id`, `source`, `embeded_code`, `image_cover`, `ref_project`, `order_elt`) VALUES
(2, NULL, 'e2LkglavLRs', 'Modvideo_image_cover58e213b840b2c.jpg', 18, 3);

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `isDisplay` int(1) DEFAULT NULL,
  `isStared` int(11) DEFAULT NULL,
  `stared_description_text` text,
  `stared_order` int(11) DEFAULT NULL,
  `stared_cover_image` varchar(255) DEFAULT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `location` varchar(127) DEFAULT NULL,
  `project_type` varchar(127) DEFAULT NULL,
  `architect` varchar(127) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  `description_seo` varchar(140) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `project`
--

INSERT INTO `project` (`id`, `title`, `isDisplay`, `isStared`, `stared_description_text`, `stared_order`, `stared_cover_image`, `cover_image`, `location`, `project_type`, `architect`, `ordre`, `description_seo`, `slug`) VALUES
(18, 'Msheireb', 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, \r\nsed do eiusmod tempor incididunt ut labore.', 2, 'Project_stared_cover_image58bfe548a7634.jpg', 'Project_cover_image58bfe5571b355.jpg', 'Doha, Qatar', 'Msheireb', 'John Doe', 2, 'mots clés SEO', NULL),
(19, 'Four Seasons', 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, \r\nsed do eiusmod tempor incididunt ut labore.', 1, 'Project_stared_cover_image58bfe6209bc14.jpg', 'Project_cover_image58bfe6209bffc.jpg', 'Paris', 'Building', 'John Doe', 1, '', NULL),
(20, 'Top''s 10 of best VIDEOS ev4r', 1, 0, '', NULL, '', 'Project_cover_image58bfe67021547.jpg', 'Madrid', 'Building', 'John Dae', 3, '', NULL),
(23, 'sdfsdf', 0, 0, '', NULL, '', '', '', '', '', 4, '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sliderimage`
--

CREATE TABLE IF NOT EXISTS `sliderimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_slider` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_slider` (`ref_slider`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `sliderimage`
--

INSERT INTO `sliderimage` (`id`, `ref_slider`, `image`, `order_elt`) VALUES
(10, 5, 'Sliderimage_image58bfe4785fe02.jpg', 1),
(11, 5, 'Sliderimage_image58bfe4a0d4e55.jpg', 2);

-- --------------------------------------------------------

--
-- Structure de la table `staticimage`
--

CREATE TABLE IF NOT EXISTS `staticimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `staticimage`
--

INSERT INTO `staticimage` (`id`, `title`, `getTitle`, `value`) VALUES
(13, 'Image 1', 'about_image_1', 'static_image58bea7e0c70a7.jpg'),
(14, 'Image 2', 'about_image_2', 'static_image58bdbdeb0c67a.jpg'),
(15, 'Image 3', 'about_image_3', 'static_image58bdbdf1a592d.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `staticseo`
--

CREATE TABLE IF NOT EXISTS `staticseo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `getTitle` varchar(63) NOT NULL,
  `value` varchar(140) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `staticseo`
--

INSERT INTO `staticseo` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Contact SEO', 'contact_seo', 'contact page'),
(2, 'SEO description', 'about_seo', 'about page');

-- --------------------------------------------------------

--
-- Structure de la table `staticslider`
--

CREATE TABLE IF NOT EXISTS `staticslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `getTitle` varchar(127) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `staticslider`
--

INSERT INTO `staticslider` (`id`, `title`, `getTitle`) VALUES
(1, 'Slider 1', 'about_slider_1');

-- --------------------------------------------------------

--
-- Structure de la table `staticsliderimage`
--

CREATE TABLE IF NOT EXISTS `staticsliderimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_slider` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_slider` (`ref_slider`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `staticsliderimage`
--

INSERT INTO `staticsliderimage` (`id`, `ref_slider`, `image`, `order_elt`) VALUES
(4, 1, 'Staticsliderimage_image 58bdc283d6df9.jpg', 1),
(5, 1, 'Staticsliderimage_image 58bdc28ae7d15.jpg', 2);

-- --------------------------------------------------------

--
-- Structure de la table `statictext`
--

CREATE TABLE IF NOT EXISTS `statictext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `statictext`
--

INSERT INTO `statictext` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Text bloc 1', 'about_text_1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit !!!!'),
(2, 'Text bloc 2', 'about_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit :):)'),
(3, 'Text bloc 3', 'about_text_3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ><');

-- --------------------------------------------------------

--
-- Structure de la table `statictitle`
--

CREATE TABLE IF NOT EXISTS `statictitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `statictitle`
--

INSERT INTO `statictitle` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Title bloc 1', 'about_title_1', 'Approche !'),
(2, 'Title bloc 2', 'about_title_2', 'Elegant and sustainable'),
(3, 'Title bloc 3', 'about_title_3', 'It''s about being different !');

-- --------------------------------------------------------

--
-- Structure de la table `staticvideo`
--

CREATE TABLE IF NOT EXISTS `staticvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `staticvideo`
--

INSERT INTO `staticvideo` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Video', 'about_video_1', '221348419');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `modimage`
--
ALTER TABLE `modimage`
  ADD CONSTRAINT `modimage_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modslider`
--
ALTER TABLE `modslider`
  ADD CONSTRAINT `modslider_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modtext`
--
ALTER TABLE `modtext`
  ADD CONSTRAINT `modText_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modtitle`
--
ALTER TABLE `modtitle`
  ADD CONSTRAINT `modtitle_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modvideo`
--
ALTER TABLE `modvideo`
  ADD CONSTRAINT `modVideo_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sliderimage`
--
ALTER TABLE `sliderimage`
  ADD CONSTRAINT `sliderimage_ibfk_1` FOREIGN KEY (`ref_slider`) REFERENCES `modslider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `staticsliderimage`
--
ALTER TABLE `staticsliderimage`
  ADD CONSTRAINT `staticsliderimage_ibfk_1` FOREIGN KEY (`ref_slider`) REFERENCES `staticslider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
