# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# H�te: 127.0.0.1 (MySQL 5.5.38)
# Base de donn�es: burton_studio
# Temps de g�n�ration: 2017-11-10 15:28:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adress_line_1` varchar(255) DEFAULT NULL,
  `adress_line_2` varchar(255) DEFAULT NULL,
  `adress_country` varchar(31) DEFAULT NULL,
  `phone` varchar(31) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;

INSERT INTO `contact` (`id`, `adress_line_1`, `adress_line_2`, `adress_country`, `phone`, `email`)
VALUES
	(1,'307 S Cedros Ave,','Solana Beacj, CA 92075','USA','+1 858 794 7204','mon@email.com');

/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modimage`;

CREATE TABLE `modimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  CONSTRAINT `modimage_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modimage` WRITE;
/*!40000 ALTER TABLE `modimage` DISABLE KEYS */;

INSERT INTO `modimage` (`id`, `image`, `ref_project`, `order_elt`)
VALUES
	(23,'Modimage_image 58bfe4c8d62fe.jpg',18,5),
	(30,'Modimage_image 58e2db21381fb.jpg',23,2),
	(31,'Modimage_image 5940500539131.jpg',20,1),
	(32,'Modimage_image 5940501ebb506.jpg',20,5);

/*!40000 ALTER TABLE `modimage` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modimg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modimg`;

CREATE TABLE `modimg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modimg` WRITE;
/*!40000 ALTER TABLE `modimg` DISABLE KEYS */;

INSERT INTO `modimg` (`id`, `image`, `ref_project`, `order_elt`)
VALUES
	(1,'Modimg_image5a04dda984116.JPG',18,6);

/*!40000 ALTER TABLE `modimg` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modlargetitle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modlargetitle`;

CREATE TABLE `modlargetitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modlargetitle` WRITE;
/*!40000 ALTER TABLE `modlargetitle` DISABLE KEYS */;

INSERT INTO `modlargetitle` (`id`, `title`, `ref_project`, `order_elt`)
VALUES
	(1,'sdfsdf',24,3);

/*!40000 ALTER TABLE `modlargetitle` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modslider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modslider`;

CREATE TABLE `modslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  CONSTRAINT `modslider_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modslider` WRITE;
/*!40000 ALTER TABLE `modslider` DISABLE KEYS */;

INSERT INTO `modslider` (`id`, `ref_project`, `order_elt`)
VALUES
	(5,18,1);

/*!40000 ALTER TABLE `modslider` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modspace
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modspace`;

CREATE TABLE `modspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modspace` WRITE;
/*!40000 ALTER TABLE `modspace` DISABLE KEYS */;

INSERT INTO `modspace` (`id`, `ref_project`, `order_elt`)
VALUES
	(1,24,2);

/*!40000 ALTER TABLE `modspace` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modtext
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modtext`;

CREATE TABLE `modtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  KEY `ref_project_2` (`ref_project`),
  CONSTRAINT `modText_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modtext` WRITE;
/*!40000 ALTER TABLE `modtext` DISABLE KEYS */;

INSERT INTO `modtext` (`id`, `text`, `ref_project`, `order_elt`)
VALUES
	(16,'Musheireb is located in the heart of Doha. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',18,2),
	(19,'qsdqsdaaazzzz',18,4),
	(20,'qsdq',20,3);

/*!40000 ALTER TABLE `modtext` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modtitle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modtitle`;

CREATE TABLE `modtitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  CONSTRAINT `modtitle_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modtitle` WRITE;
/*!40000 ALTER TABLE `modtitle` DISABLE KEYS */;

INSERT INTO `modtitle` (`id`, `title`, `ref_project`, `order_elt`)
VALUES
	(12,'qsdqsd',20,4),
	(13,'sdf',20,6),
	(15,'Title',19,3);

/*!40000 ALTER TABLE `modtitle` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modvideo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modvideo`;

CREATE TABLE `modvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `embeded_code` varchar(255) DEFAULT NULL,
  `image_cover` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  CONSTRAINT `modVideo_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modvideo` WRITE;
/*!40000 ALTER TABLE `modvideo` DISABLE KEYS */;

INSERT INTO `modvideo` (`id`, `source`, `embeded_code`, `image_cover`, `ref_project`, `order_elt`)
VALUES
	(2,NULL,'e2LkglavLRs','Modvideo_image_cover58e213b840b2c.jpg',18,3);

/*!40000 ALTER TABLE `modvideo` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `isDisplay` int(1) DEFAULT NULL,
  `isStared` int(11) DEFAULT NULL,
  `stared_description_text` text,
  `stared_order` int(11) DEFAULT NULL,
  `stared_cover_image` varchar(255) DEFAULT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `location` varchar(127) DEFAULT NULL,
  `project_type` varchar(127) DEFAULT NULL,
  `architect` varchar(127) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  `description_seo` varchar(140) DEFAULT NULL,
  `description_text` text,
  `visual_image_1` varchar(255) DEFAULT NULL,
  `visual_image_2` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `title`, `isDisplay`, `isStared`, `stared_description_text`, `stared_order`, `stared_cover_image`, `cover_image`, `location`, `project_type`, `architect`, `ordre`, `description_seo`, `description_text`, `visual_image_1`, `visual_image_2`, `slug`)
VALUES
	(18,'Msheireb',1,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, \r\nsed do eiusmod tempor incididunt ut labore.',2,'Project_stared_cover_image5a023bb7d0359.JPG','Project_cover_image5a023bc01d63e.JPG','Doha, Qatar','Msheireb','John Doe',2,'mots clés SEO','Lorem ipsum','Project_visual_image_15a0239e6a2362.JPG','Project_visual_image_25a0239e6a2a8e.JPG',NULL),
	(19,'Four Seasons',0,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, \r\nsed do eiusmod tempor incididunt ut labore.',1,'Project_stared_cover_image58bfe6209bc14.jpg','Project_cover_image58bfe6209bffc.jpg','Paris','Building','John Doe',1,'',NULL,NULL,NULL,NULL),
	(20,'Top\'s 10 of best VIDEOS ev4r',1,0,'',NULL,'','Project_cover_image58bfe67021547.jpg','Madrid','Building','John Dae',3,'',NULL,NULL,NULL,NULL),
	(23,'sdfsdf',0,0,'',NULL,'','','','','',4,'',NULL,NULL,NULL,NULL),
	(25,'teztÉ',1,1,'',NULL,'','Project_cover_image5a023d4c614df.JPG','sdf','sdf','sdf',5,'sdf','sdf','Project_visual_image_15a023d4c6150b.JPG','Project_visual_image_25a023d4c61535.JPG',NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table sliderimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sliderimage`;

CREATE TABLE `sliderimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_slider` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_slider` (`ref_slider`),
  CONSTRAINT `sliderimage_ibfk_1` FOREIGN KEY (`ref_slider`) REFERENCES `modslider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sliderimage` WRITE;
/*!40000 ALTER TABLE `sliderimage` DISABLE KEYS */;

INSERT INTO `sliderimage` (`id`, `ref_slider`, `image`, `order_elt`)
VALUES
	(10,5,'Sliderimage_image58bfe4785fe02.jpg',1),
	(11,5,'Sliderimage_image58bfe4a0d4e55.jpg',2);

/*!40000 ALTER TABLE `sliderimage` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table staticimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staticimage`;

CREATE TABLE `staticimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `staticimage` WRITE;
/*!40000 ALTER TABLE `staticimage` DISABLE KEYS */;

INSERT INTO `staticimage` (`id`, `title`, `getTitle`, `value`)
VALUES
	(13,'Image 1','about_image_1','static_image58bea7e0c70a7.jpg'),
	(14,'Image 2','about_image_2','static_image58bdbdeb0c67a.jpg'),
	(15,'Image 3','about_image_3','static_image58bdbdf1a592d.jpg');

/*!40000 ALTER TABLE `staticimage` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table staticseo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staticseo`;

CREATE TABLE `staticseo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `getTitle` varchar(63) NOT NULL,
  `value` varchar(140) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `staticseo` WRITE;
/*!40000 ALTER TABLE `staticseo` DISABLE KEYS */;

INSERT INTO `staticseo` (`id`, `title`, `getTitle`, `value`)
VALUES
	(1,'Contact SEO','contact_seo','contact page'),
	(2,'SEO description','about_seo','about page');

/*!40000 ALTER TABLE `staticseo` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table staticslider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staticslider`;

CREATE TABLE `staticslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `getTitle` varchar(127) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `staticslider` WRITE;
/*!40000 ALTER TABLE `staticslider` DISABLE KEYS */;

INSERT INTO `staticslider` (`id`, `title`, `getTitle`)
VALUES
	(1,'Slider 1','about_slider_1');

/*!40000 ALTER TABLE `staticslider` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table staticsliderimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staticsliderimage`;

CREATE TABLE `staticsliderimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_slider` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_slider` (`ref_slider`),
  CONSTRAINT `staticsliderimage_ibfk_1` FOREIGN KEY (`ref_slider`) REFERENCES `staticslider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `staticsliderimage` WRITE;
/*!40000 ALTER TABLE `staticsliderimage` DISABLE KEYS */;

INSERT INTO `staticsliderimage` (`id`, `ref_slider`, `image`, `order_elt`)
VALUES
	(4,1,'Staticsliderimage_image 58bdc283d6df9.jpg',1),
	(5,1,'Staticsliderimage_image 58bdc28ae7d15.jpg',2);

/*!40000 ALTER TABLE `staticsliderimage` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table statictext
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statictext`;

CREATE TABLE `statictext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `statictext` WRITE;
/*!40000 ALTER TABLE `statictext` DISABLE KEYS */;

INSERT INTO `statictext` (`id`, `title`, `getTitle`, `value`)
VALUES
	(1,'Text bloc 1','about_text_1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit !!!!'),
	(2,'Text bloc 2','about_text_2','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit :):)'),
	(3,'Text bloc 3','about_text_3','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ><');

/*!40000 ALTER TABLE `statictext` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table statictitle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statictitle`;

CREATE TABLE `statictitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `statictitle` WRITE;
/*!40000 ALTER TABLE `statictitle` DISABLE KEYS */;

INSERT INTO `statictitle` (`id`, `title`, `getTitle`, `value`)
VALUES
	(1,'Title bloc 1','about_title_1','Approche !'),
	(2,'Title bloc 2','about_title_2','Elegant and sustainable'),
	(3,'Title bloc 3','about_title_3','It\'s about being different !');

/*!40000 ALTER TABLE `statictitle` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table staticvideo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staticvideo`;

CREATE TABLE `staticvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `staticvideo` WRITE;
/*!40000 ALTER TABLE `staticvideo` DISABLE KEYS */;

INSERT INTO `staticvideo` (`id`, `title`, `getTitle`, `value`)
VALUES
	(1,'Video','about_video_1','221348419');

/*!40000 ALTER TABLE `staticvideo` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
