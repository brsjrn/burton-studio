-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 03 Mars 2017 à 20:10
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `burton_studio`
--

-- --------------------------------------------------------

--
-- Structure de la table `modslider`
--

CREATE TABLE IF NOT EXISTS `modslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `modtext`
--

CREATE TABLE IF NOT EXISTS `modtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  KEY `ref_project_2` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Contenu de la table `modtext`
--

INSERT INTO `modtext` (`id`, `text`, `ref_project`, `order_elt`) VALUES
(100, 'Text 1', 42, 2),
(101, 'Text 2', 42, 4);

-- --------------------------------------------------------

--
-- Structure de la table `modtitle`
--

CREATE TABLE IF NOT EXISTS `modtitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Contenu de la table `modtitle`
--

INSERT INTO `modtitle` (`id`, `title`, `ref_project`, `order_elt`) VALUES
(48, 'Titre 1', 42, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modvideo`
--

CREATE TABLE IF NOT EXISTS `modvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `embeded_code` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `isStared` int(11) DEFAULT NULL,
  `stared_description_text` text,
  `stared_order` int(11) DEFAULT NULL,
  `stared_cover_image` varchar(255) DEFAULT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `location` varchar(127) DEFAULT NULL,
  `project_type` varchar(127) DEFAULT NULL,
  `architect` varchar(127) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Contenu de la table `project`
--

INSERT INTO `project` (`id`, `title`, `isStared`, `stared_description_text`, `stared_order`, `stared_cover_image`, `cover_image`, `location`, `project_type`, `architect`, `ordre`) VALUES
(42, 'Project', NULL, '<p>fssfsdf</p>\r\n', NULL, '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `staticimage`
--

CREATE TABLE IF NOT EXISTS `staticimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `staticimage`
--

INSERT INTO `staticimage` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Image 1', 'about_image_1', ''),
(2, 'Image 2', 'about_image_2', ''),
(3, 'Image 3', 'about_image_3', ''),
(4, 'Image 4', 'about_image_4', '');

-- --------------------------------------------------------

--
-- Structure de la table `statictext`
--

CREATE TABLE IF NOT EXISTS `statictext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `statictext`
--

INSERT INTO `statictext` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Text bloc 1', 'about_text_1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit !!'),
(2, 'Text bloc 2', 'about_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit :)'),
(3, 'Text bloc 3', 'about_text_3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ><');

-- --------------------------------------------------------

--
-- Structure de la table `statictitle`
--

CREATE TABLE IF NOT EXISTS `statictitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `statictitle`
--

INSERT INTO `statictitle` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Title bloc 1', 'about_title_1', 'Approch'),
(2, 'Title bloc 2', 'about_title_2', 'Elegant and sustainable'),
(3, 'Title bloc 3', 'about_title_3', 'It''s about being different');

-- --------------------------------------------------------

--
-- Structure de la table `staticvideo`
--

CREATE TABLE IF NOT EXISTS `staticvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `getTitle` varchar(63) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `staticvideo`
--

INSERT INTO `staticvideo` (`id`, `title`, `getTitle`, `value`) VALUES
(1, 'Video', 'about_video_1', '');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `modslider`
--
ALTER TABLE `modslider`
  ADD CONSTRAINT `modslider_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modtext`
--
ALTER TABLE `modtext`
  ADD CONSTRAINT `modText_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modtitle`
--
ALTER TABLE `modtitle`
  ADD CONSTRAINT `modtitle_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modvideo`
--
ALTER TABLE `modvideo`
  ADD CONSTRAINT `modVideo_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
