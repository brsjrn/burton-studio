<?php

class StaticimageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(isset($_GET['oldurl']) && $_GET['oldurl'] != '') {
			$this->redirect($_GET['oldurl']);
		}

		$this->render('view',array(
			'model'=>$this->loadModel($id)
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Staticimage;

		
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Staticimage']))
		{
			$model->attributes=$_POST['Staticimage'];

			// Manage order
			
			// Manage links
						
			// Manage images upload
			$tmp_image = CUploadedFile::getInstance($model, 'value');

			if(isset($tmp_image)) {
	            $model->value = 'static'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	        }
			            
			if($model->save()) {
				if(isset($tmp_image)) {
					if(!$tmp_image->saveAs('images/static/'. $model->value)) {
                         throw new ExceptionClass('Problem static image upload');
                     }
				}

				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$currentId = $id;

		if($id == 0) {
			$currentId = $_POST['id'];
		}

		$model=$this->loadModel($currentId);

		if(isset($_POST['Staticimage']))
		{
			$model->attributes=$_POST['Staticimage'];

			// Manage image
			$tmp_image = CUploadedFile::getInstance($model, 'value');

			if(isset($tmp_image)) {
	            $model->value = 'static'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	        }
	        if(!isset($tmp_image) && isset($old_image)) {
	            $model->value = $old_image;
	        }

			if($model->save()) {

				if(isset($tmp_image)) {
					if(!$tmp_image->saveAs('images/static/'. $model->value)) {
	                     throw new ExceptionClass('Problem static image upload');
	                 }
	                 if(isset($old_image) && strlen($old_image)>0) {
	                    unlink('images/static/' . $old_image);
	                }
				}

				if (Yii::app()->request->isAjaxRequest)
	            {
	                echo CJSON::encode(array(
	                    'status'=>'success', 
	                    'div'=>"Title successfully edited",
	                    'model'=> array(
	                    	'id' => $model->id,
	                    	'value' => $model->value)
	                    ));
	                exit;               
	            }
	            else {
	            	$oldurl = Yii::app()->request->urlReferrer;
	            	//$oldurl = "caca";
	                $this->redirect(array('view','id'=>$model->id, 'oldurl' => $oldurl));
	            }
	            //$this->redirect(array('view','id'=>$model->id));
			}
		}

		if (Yii::app()->request->isAjaxRequest)
	    {
	        echo CJSON::encode(array(
	            'status'=>'failure', 
	            'div'=>$this->renderPartial('_form', array('model'=>$model), true)));
	        exit;               
	    }
	    else {
	        $this->render('update',array('model'=>$model));
	    }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 
		
                
                

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Staticimage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Staticimage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Staticimage']))
			$model->attributes=$_GET['Staticimage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Staticimage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Staticimage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Staticimage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='staticimage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Staticimage::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Staticimage::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Staticimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Staticimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Staticimage::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Staticimage::model()->findAll($criteria);
        
        return $listModels;
    }
}
