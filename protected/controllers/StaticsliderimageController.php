<?php

class StaticsliderimageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','adminById','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
	    $model = $this->loadModel($id);
	    $parent = Staticslider::model()->findByPk($model->ref_slider);
	    $this->render('view', array(
	        'model' => $model,
	        'parent' => $parent
	    ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($refModelId)
	{
		$model=new Staticsliderimage;

		$parent = Staticslider::model()->findByPk($refModelId); 
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Staticsliderimage']))
		{
			$model->attributes=$_POST['Staticsliderimage'];

			// Manage order
			
			// Manage links
						

			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Staticsliderimage'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
						            
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('images/Staticsliderimages/'. $model->image)) {
                                         throw new ExceptionClass('Problem Staticsliderimage image upload');
                                     }
								}
											$this->redirect(array('adminById', 'refModelId' => $refModelId));
			}
		}

		
					$this->render('create',array(
						'model' => $model,
						'parent' => $parent
					));
					}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$parent = Staticslider::model()->findByPk($model->ref_slider);
		
		// Current images
		
						$old_image = $model->image;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Staticsliderimage']))
		{
			$model->attributes=$_POST['Staticsliderimage'];

			// Manage order
			
			// Manage links
			
			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Staticsliderimage'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
	                        if(!isset($tmp_image) && isset($old_image)) {
	                            $model->image = $old_image;
	                        }
						
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('images/Staticsliderimages/'. $model->image)) {
                                         throw new ExceptionClass('Problem Staticsliderimage image upload');
                                     }
                                     if(isset($old_image) && strlen($old_image)>0) {
	                                    unlink('images/Staticsliderimages/' . $old_image);
	                                }
								}
											$this->redirect(array('adminById', 'refModelId' => $parent->id));
			}
		}

		
					$this->render('update',array(
						'model' => $model,
						'parent' => $parent
					));
					}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$refModelId = $model->ref_slider;
		$model->delete();

		if(isset($model->image) && $model->image != '') {
            unlink('images/Staticsliderimages/'. $model->image);
        }
					        
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('adminById', 'refModelId' => $refModelId));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Staticsliderimage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Staticsliderimage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Staticsliderimage']))
			$model->attributes=$_GET['Staticsliderimage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminById($refModelId) {
	    $model = new Staticsliderimage('search');
	    $model->unsetAttributes();  // clear any default values
	    if (isset($_GET['Staticsliderimage']))
	        $model->attributes = $_GET['Staticsliderimage'];

	    $parent = Staticslider::model()->findByPk($refModelId);

	    $this->render('admin', array(
	        'model' => $model,
	        'parent' => $parent,
	    ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Staticsliderimage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Staticsliderimage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Staticsliderimage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='staticsliderimage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Staticsliderimage::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Staticsliderimage::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Staticsliderimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Staticsliderimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Staticsliderimage::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Staticsliderimage::model()->findAll($criteria);
        
        return $listModels;
    }
}
