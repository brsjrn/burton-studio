<?php

class ModlargetitleController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','updateOrder','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Modlargetitle;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Modlargetitle']))
		{
			$model->attributes=$_POST['Modlargetitle'];
			if($model->save()) {
				if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>"Large title successfully added",
                        'model'=> array(
                        	'id' => $model->id,
                        	'title' => $model->title)
                        ));
                    exit;               
                }
                else {
                    $this->redirect(array('view','id'=>$model->id));
                }
			}
		}

		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial('_form', array('model'=>$model), true)));
            exit;               
        }
        else {
            $this->render('create',array('model'=>$model,));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$currentId = $id;

		if($id == 0) {
			$currentId = $_POST['id'];
		}

		$model=$this->loadModel($currentId);

		if(isset($_POST['Modlargetitle']))
		{
			$model->attributes=$_POST['Modlargetitle'];
			if($model->save()) {
				if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>"Title successfully edited",
                        'model'=> array(
                        	'id' => $model->id,
                        	'title' => $model->title)
                        ));
                    exit;               
                }
                else {
                    $this->redirect(array('view','id'=>$model->id));
                }
			}
		}

		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial('_form', array('model'=>$model), true)));
            exit;               
        }
        else {
            $this->render('update',array('model'=>$model,));
        }
	}

	public function actionUpdateOrder($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['order'])) {
			$newOrder = $_POST['order'];

			$model->order_elt = $newOrder;

			$model->save();

			echo CJSON::encode(array('status'=>'success', 'newOrder' => $newOrder));
		} else {
			echo CJSON::encode(array('status'=>'falure'));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$currentId = $id;

		if($id == 0) {
			$currentId = $_POST['id'];
		}

		$this->loadModel($currentId)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'success',
                'model'=> array(
                	'id' => $currentId)
                ));
            exit;             
        }
        else {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        /*
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		*/
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Modlargetitle');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Modlargetitle('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Modlargetitle']))
			$model->attributes=$_GET['Modlargetitle'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Modlargetitle the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Modlargetitle::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Modlargetitle $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='modlargetitle-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Modlargetitle::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Modlargetitle::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Modlargetitle::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Modlargetitle::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Modlargetitle::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Modlargetitle::model()->findAll($criteria);
        
        return $listModels;
    }
}
