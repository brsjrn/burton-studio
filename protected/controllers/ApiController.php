<?php 

	class ApiController extends Controller
	{
	    // Members
	    /**
	     * Key which has to be in HTTP USERNAME and PASSWORD headers 
	     */
	    Const APPLICATION_ID = 'ASCCPE';
	 
	    /**
	     * Default response format
	     * either 'json' or 'xml'
	     */
	    private $format = 'json';

	    /**
	     * @return array action filters
	     */
	    public function filters()
	    {
	            return array();
	    }
	 
	 	//--------------
	    //	API ACTIONS
		//--------------

		// List all data
	    public function actionAll()
	    {
	    	// Get dataProjects
	    	$datasProjects = array();

	    	$criteriaProject = new CDbCriteria();
	    	$criteriaProject->addCondition('isDisplay=:isDisplay');
	    	$criteriaProject->params = array(
	    	        ':isDisplay'=>1
	    	);
            $projects = Project::model()->findAll($criteriaProject);
            foreach ($projects as $key => $model) {
            	//$modules = $this->getOrderedModules($model->id);
            	$model->slug = $this->Slug($model->title);
            
            	array_push($datasProjects, array(
            		'project' => array(
            			'model' => $model,
            			//'modules' => $modules,
        			)
        		));
            }

            // Get dataHome
            $projects_home = $this->getStaredProjects();
            foreach ($projects_home as $key => $model) {
            	$model->slug = $this->Slug($model->title);
            }

            $datasHome = ['projects' => $projects_home];

            // Get dataAbout
            $datasAbout = $this->getAboutFields();

    	    // About SEO
            $criteria = new CDbCriteria();
            $criteria->addCondition("getTitle=:getTitle");
            $criteria->params = array(
                ':getTitle'=>'contact_seo'
            );

            // Get dataAbout
            $datasContact = array(
            	'contacts' => Contact::model()->findAll(),
            	'description_seo' => Staticseo::model()->find($criteria)
        	);

    	    // Did we get some results?
	    	$status = 1;
	        $this->_sendResponse(200, CJSON::encode(array(
	        	'status'=>$status,
	        	'dataHome'=>$datasHome,
	        	'datasAbout'=>$datasAbout,
	        	'datasContact'=>$datasContact,
	        	'datasProjects'=>$datasProjects
	        )));
	    }

		// List all models
	    public function actionList()
	    {
	    	$datas = array();

	    	// Get the respective model instance
    	    switch($_GET['model'])
    	    {
    	        case 'projects':
    	            $models = Project::model()->findAll(array('order'=>'ordre'));
    	            foreach ($models as $key => $model) {
    	            	//$modules = $this->getOrderedModules($model->id);
    	            	$model->slug = $this->Slug($model->title);
    	            
    	            	array_push($datas, array(
        		    		'project' => array(
        		    			'model' => $model,
        		    			//'modules' => $modules
        					)
	            		));
    	            }
    	            break;
    	        default:
    	            // Model not implemented error
	            	$status = 2;
	                $this->_sendResponse(200, CJSON::encode(array(
	                	'status'=>$status,
	                )));
    	    }

    	    // Did we get some results?
    	    if(empty($models)) {
    	        // No
    	        $status = 0;
    	        $this->_sendResponse(200, 
    	                sprintf('No items where found for model <b>%s</b>', $_GET['model']) );
    	    } else {
    	        // Send the response
	        	$status = 1;
	            $this->_sendResponse(200, CJSON::encode(array(
	            	'status'=>$status,
	            	'datas'=>$datas
	            )));
    	    }
	    }

	    // Get specific model BY id
	    public function actionView()
	    {
	    	$status; // 0 => no data / 1 => success / 2 => no model
	    	$datas = array();

	    	// Check if id was submitted via GET
    	    if(!isset($_GET['id']))
    	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
    	 
    	    switch($_GET['model'])
    	    {
    	        // Find respective model    
    	        case 'project':
    	        	// Get model
    	            $model = Project::model()->findByPk($_GET['id']);

    	            // Get text modules
    	            if(!is_null($model)) {
	    	            //$modules = $this->getOrderedModules($model->id);
	    	            $model->slug = $this->Slug($model->title);
	    	        
    	            	array_push($datas, array(
    	            		'model' => $model,
        		    		//'modules' => $modules
                		));
	    	        }

    	            break;
    	        default:
	        		$status = 2;
	        	    $this->_sendResponse(200, CJSON::encode(array(
	        	    	'status'=>$status,
	        	    )));
    	    }

    	    // Did we find the requested model? If not, raise an error
    	    if(is_null($model)) {
    	    	$status = 0;
    	        $this->_sendResponse(200, CJSON::encode(array(
    	        	'status'=>$status,
    	        )));
    	    } else {
    	    	$status = 1;
    	        $this->_sendResponse(200, CJSON::encode(array(
    	        	'status'=>$status,
    	        	'datas'=>$datas
    	        )));
    	    }
	    }

	    // Get specific data BY project id
	    public function actionData()
	    {
	    	$status; // 0 => no data / 1 => success / 2 => no model
	    	$data;

	    	// Check if id was submitted via GET
    	    if(!isset($_GET['id'])) {
    	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
    	    } else {
    	    	$model = Project::model()->findByPk($_GET['id']);
    	    }

            if(!isset($_GET['data'])) {
    	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
    	    } else {
    	    	if(isset($model)) {
    	    		switch ($_GET['data']) {
    	    			case 'id':
    	    				$data = $model->id;
    	    				break;
	    				case 'title':
    	    				$data = $model->title;
    	    				break;
	    				case 'isStared':
    	    				$data = $model->isStared;
    	    				break;
	    				case 'stared_description_text':
    	    				$data = $model->stared_description_text;
    	    				break;
	    				case 'stared_order':
    	    				$data = $model->stared_order;
    	    				break;
	    				case 'stared_cover_image':
    	    				$data = $model->stared_cover_image;
    	    				break;
	    				case 'cover_image':
    	    				$data = $model->cover_image;
    	    				break;
	    				case 'location':
    	    				$data = $model->location;
    	    				break;
    					case 'project_type':
    	    				$data = $model->project_type;
    	    				break;
	    				case 'architect':
    	    				$data = $model->architect;
    	    				break;
	    				case 'ordre':
    	    				$data = $model->ordre;
    	    				break;
	    				case 'description_seo':
	    					$data = $model->description_seo;
	    					break;
    					case 'slug':
    	    				$data = $this->Slug($model->title);
    	    				break;
    	    			default:
    	    				break;
    	    		}
    	    	}
    	    }

    	    // Did we find the requested model? If not, raise an error
    	    if(is_null($data)) {
    	    	$status = 0;
    	        $this->_sendResponse(200, CJSON::encode(array(
    	        	'status'=>$status,
    	        )));
    	    } else {
    	    	$status = 1;
    	        $this->_sendResponse(200, CJSON::encode(array(
    	        	'status'=>$status,
    	        	'data'=>$data
    	        )));
    	    }
	    }

		// Pages
	    public function actionPage()
	    {
	    	$contacts = Contact::model()->findAll();

	    	// Get the respective model instance
    	    switch($_GET['page'])
    	    {
    	        case 'about':
    	            $fields = $this->getAboutFields();

    	            // Did we get some results?
    	            if(is_null($fields)) {
    	            	$status = 0;
    	                $this->_sendResponse(200, CJSON::encode(array(
    	                	'status'=>$status,
    	                )));
    	            } else {
    	            	$status = 1;
    	                $this->_sendResponse(200, CJSON::encode(array(
    	                	'status'=>$status,
    	                	'fields'=>$fields,
    	                	'contacts'=>$contacts
    	                )));
    	            }
    	            break;
	            case 'home':
	                $projects = $this->getStaredProjects();

	                // Did we get some results?
	                if(is_null($projects)) {
	                	$status = 0;
	                    $this->_sendResponse(200, CJSON::encode(array(
	                    	'status'=>$status,
	                    )));
	                } else {
	                	$status = 1;
	                    $this->_sendResponse(200, CJSON::encode(array(
	                    	'status'=>$status,
	                    	'projects'=>$projects,
	                    	'contacts'=>$contacts
	                    )));
	                }
	                break;
    	        default:
    	            // Model not implemented error
	            	$status = 2;
	                $this->_sendResponse(200, CJSON::encode(array(
	                	'status'=>$status,
	                )));
    	    }
	    }

     	//---------------
        //	Other methods
    	//---------------
	    
	    // Send response
	    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
	    {
	        // set the status
	        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	        header($status_header);
	        // and the content type
	        header('Content-type: ' . $content_type);
	     
	        // pages with body are easy
	        if($body != '')
	        {
	            // send the body
	            echo $body;
	        }
	        // we need to create the body if none is passed
	        else
	        {
	            // create some body messages
	            $message = '';
	     
	            // this is purely optional, but makes the pages a little nicer to read
	            // for your users.  Since you won't likely send a lot of different status codes,
	            // this also shouldn't be too ponderous to maintain
	            switch($status)
	            {
	                case 401:
	                    $message = 'You must be authorized to view this page.';
	                    break;
	                case 404:
	                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
	                    break;
	                case 500:
	                    $message = 'The server encountered an error processing your request.';
	                    break;
	                case 501:
	                    $message = 'The requested method is not implemented.';
	                    break;
	            }
	     
	            // servers don't always have a signature turned on 
	            // (this is an apache directive "ServerSignature On")
	            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
	     
	            // this should be templated in a real-world solution
	            $body = '
				    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
				    <html>
				    <head>
				        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
				        <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
				    </head>
				    <body>
				        <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
				        <p>' . $message . '</p>
				        <hr />
				        <address>' . $signature . '</address>
				    </body>
				    </html>';
	     
	            echo $body;
	        }
	        Yii::app()->end();
	    }

	    // Get status code
	    private function _getStatusCodeMessage($status)
	    {
	        // these could be stored in a .ini file and loaded
	        // via parse_ini_file()... however, this will suffice
	        // for an example
	        $codes = Array(
	            200 => 'OK',
	            400 => 'Bad Request',
	            401 => 'Unauthorized',
	            402 => 'Payment Required',
	            403 => 'Forbidden',
	            404 => 'Not Found',
	            500 => 'Internal Server Error',
	            501 => 'Not Implemented',
	        );
	        return (isset($codes[$status])) ? $codes[$status] : '';
	    }

		// Get about fields
		protected function getAboutFields() {

	    	// About video 1
		    $criteria11 = new CDbCriteria();
		    $criteria11->addCondition("getTitle=:getTitle");
		    $criteria11->params = array(
		        ':getTitle'=>'about_video_1'
		    );
		    $fields['about_video_1'] = Staticvideo::model()->find($criteria11);

			// About title 1
		    $criteria1 = new CDbCriteria();
		    $criteria1->addCondition("getTitle=:getTitle");
		    $criteria1->params = array(
		        ':getTitle'=>'about_title_1'
		    );
		    $fields['about_title_1'] = Statictitle::model()->find($criteria1);

		    // About text 1
	        $criteria4 = new CDbCriteria();
	        $criteria4->addCondition("getTitle=:getTitle");
	        $criteria4->params = array(
	            ':getTitle'=>'about_text_1'
	        );
	        $fields['about_text_1'] = Statictext::model()->find($criteria4);

	    	// About image 1
		    $criteria7 = new CDbCriteria();
		    $criteria7->addCondition("getTitle=:getTitle");
		    $criteria7->params = array(
		        ':getTitle'=>'about_image_1'
		    );
		    $fields['about_image_1'] = Staticimage::model()->find($criteria7);

	    	// About image 2
		    $criteria8 = new CDbCriteria();
		    $criteria8->addCondition("getTitle=:getTitle");
		    $criteria8->params = array(
		        ':getTitle'=>'about_image_2'
		    );
		    $fields['about_image_2'] = Staticimage::model()->find($criteria8);

	    	// About title 2
		    $criteria2 = new CDbCriteria();
		    $criteria2->addCondition("getTitle=:getTitle");
		    $criteria2->params = array(
		        ':getTitle'=>'about_title_2'
		    );
		    $fields['about_title_2'] = Statictitle::model()->find($criteria2);

		    // About text 2
	        $criteria5 = new CDbCriteria();
	        $criteria5->addCondition("getTitle=:getTitle");
	        $criteria5->params = array(
	            ':getTitle'=>'about_text_2'
	        );
	        $fields['about_text_2'] = Statictext::model()->find($criteria5);

	    	// About Slider
		    $criteria10 = new CDbCriteria();
		    $criteria10->addCondition("getTitle=:getTitle");
		    $criteria10->params = array(
		        ':getTitle'=>'about_slider_1'
		    );
		    $slider1 = Staticslider::model()->find($criteria10);
		    //$fields['about_slider_id'] = $slider1;

		    $criteria11 = new CDbCriteria();
		    $criteria11->addCondition("ref_slider=:ref_slider");
		    $criteria11->params = array(
		        ':ref_slider'=>$slider1->id
		    );
		    $fields['about_slider_1'] = Staticsliderimage::model()->findAll($criteria11);

		    //fields['about_slider_1']

	    	// About title 3
	        $criteria3 = new CDbCriteria();
	        $criteria3->addCondition("getTitle=:getTitle");
	        $criteria3->params = array(
	            ':getTitle'=>'about_title_3'
	        );
	        $fields['about_title_3'] = Statictitle::model()->find($criteria3);

	    	// About text 3
	        $criteria6 = new CDbCriteria();
	        $criteria6->addCondition("getTitle=:getTitle");
	        $criteria6->params = array(
	            ':getTitle'=>'about_text_3'
	        );
	        $fields['about_text_3'] = Statictext::model()->find($criteria6);

	    	// About image 3
		    $criteria9 = new CDbCriteria();
		    $criteria9->addCondition("getTitle=:getTitle");
		    $criteria9->params = array(
		        ':getTitle'=>'about_image_3'
		    );
		    $fields['about_image_3'] = Staticimage::model()->find($criteria9);

    	    // About SEO
            $criteria12 = new CDbCriteria();
            $criteria12->addCondition("getTitle=:getTitle");
            $criteria12->params = array(
                ':getTitle'=>'about_seo'
            );
            $fields['description_seo'] = Staticseo::model()->find($criteria12);

		    return $fields;
		}

		protected function getOrderedModules($id)
		{
			$orderedModules = array();

			// -- Titles
			$criteria1 = new CDbCriteria();
			$criteria1->addCondition('ref_project=:ref_project');
			$criteria1->params = array(
			        ':ref_project'=>$id
			);
			$listModTitles = Modtitle::model()->findAll($criteria1);

			// -- Text
			$criteria2 = new CDbCriteria();
			$criteria2->addCondition('ref_project=:ref_project');
			$criteria2->params = array(
			        ':ref_project'=>$id
			);
			$listModTexts = Modtext::model()->findAll($criteria2);

			// -- Videos
			$criteria3 = new CDbCriteria();
			$criteria3->addCondition('ref_project=:ref_project');
			$criteria3->params = array(
			        ':ref_project'=>$id
			);
			$listModVideos = Modvideo::model()->findAll($criteria3);

			// -- Slider
			$criteria4 = new CDbCriteria();
			$criteria4->addCondition('ref_project=:ref_project');
			$criteria4->params = array(
			        ':ref_project'=>$id
			);
			$listModSliders = Modslider::model()->findAll($criteria4);

			// -- Images
			$criteria5 = new CDbCriteria();
			$criteria5->addCondition('ref_project=:ref_project');
			$criteria5->params = array(
			        ':ref_project'=>$id
			);
			$listModImages = Modimage::model()->findAll($criteria5);

			// -- Imgs
			$criteria6 = new CDbCriteria();
			$criteria6->addCondition('ref_project=:ref_project');
			$criteria6->params = array(
			        ':ref_project'=>$id
			);
			$listModImgs = Modimg::model()->findAll($criteria6);

			// -- Large title
			$criteria7 = new CDbCriteria();
			$criteria7->addCondition('ref_project=:ref_project');
			$criteria7->params = array(
			        ':ref_project'=>$id
			);
			$listModLargeTitles = Modlargetitle::model()->findAll($criteria7);

			// -- Space
			$criteria8 = new CDbCriteria();
			$criteria8->addCondition('ref_project=:ref_project');
			$criteria8->params = array(
			        ':ref_project'=>$id
			);
			$listModSpaces = Modspace::model()->findAll($criteria8);

			
			foreach ($listModTitles as $key => $modTitle) {
				$cpt = 0;

				array_push($orderedModules, $modTitle);
			}

			foreach ($listModTexts as $key => $modText) {
				$cpt = 0;

				array_push($orderedModules, $modText);
			}

			foreach ($listModVideos as $key => $modVideo) {
				$cpt = 0;

				array_push($orderedModules, $modVideo);
			}

			foreach ($listModSliders as $key => $modSlider) {
				$cpt = 0;

				array_push($orderedModules, $modSlider);
			}

			foreach ($listModImages as $key => $modImage) {
				$cpt = 0;

				array_push($orderedModules, $modImage);
			}

			foreach ($listModImgs as $key => $modImg) {
				$cpt = 0;

				array_push($orderedModules, $modImg);
			}

			foreach ($listModLargeTitles as $key => $modLargeTitle) {
				$cpt = 0;

				array_push($orderedModules, $modLargeTitle);
			}

			foreach ($listModSpaces as $key => $modSpace) {
				$cpt = 0;

				array_push($orderedModules, $modSpace);
			}
			
			// Sort array
			usort($orderedModules, function($a, $b) {
			    return $a->order_elt - $b->order_elt;
			});

			// Build new structure
			$finalModuleResponse = array();

			foreach ($orderedModules as $key => $module) {
				if(get_class($module) == "Modtitle") {
					$newNode = ['type' => 'title', 'content' => $module];
				//} else if(isset($module->text)) {
				} else if(get_class($module) == "Modlargetitle") {
					$newNode = ['type' => 'large-title', 'content' => $module];
				} else if(get_class($module) == "Modspace") {
					$newNode = ['type' => 'space', 'content' => $module];
				} else if(get_class($module) == "Modtext") {
					$newNode = ['type' => 'text', 'content' => $module];
				//} else if(isset($module->image)) {
				} else if(get_class($module) == "Modimage") {
					$newNode = ['type' => 'image', 'content' => $module];
				//} else if(isset($module->embeded_code)) {
				} else if(get_class($module) == "Modimg") {
					$newNode = ['type' => 'img', 'content' => $module];
				} else if(get_class($module) == "Modtitle") {
					$newNode = ['type' => 'video', 'content' => $module];
				} else {
					$criteria = new CDbCriteria();
					$criteria->order = 'order_elt ASC';
					$criteria->addCondition('ref_slider=:ref_slider');
					$criteria->params = array(
					        ':ref_slider'=>$module->id
					);
					$listSliderImages = Sliderimage::model()->findAll($criteria);

					$newNode = ['type' => 'slider', 'content' => $module, 'images' => $listSliderImages];
				}

				array_push($finalModuleResponse, $newNode);
			}

			return $finalModuleResponse;
		}

		protected function getStaredProjects()
		{
			$criteria = new CDbCriteria();
			$criteria->order = 'stared_order ASC';
			$criteria->addCondition('isStared=:isStared');
			$criteria->params = array(
			        ':isStared'=>1
			);
			$listStaredProjects = Project::model()->findAll($criteria);

			return $listStaredProjects;
		}

		protected function remove_accent($str)
		{
		  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð',
		                'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã',
		                'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ',
		                'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ',
		                'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę',
		                'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī',
		                'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ',
		                'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ',
		                'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 
		                'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 
		                'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ',
		                'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');

		  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
		                'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c',
		                'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
		                'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D',
		                'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g',
		                'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K',
		                'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o',
		                'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S',
		                's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W',
		                'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i',
		                'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
		  return str_replace($a, $b, $str);
		}

		protected function Slug($str){
		  return mb_strtolower(preg_replace(array('/[^a-zA-Z0-9 \'-]/', '/[ -\']+/', '/^-|-$/'),
		  array('', '-', ''), $this->remove_accent($str)));
		}

	}

?>