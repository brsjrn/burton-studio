<?php

class ModtextController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','updateOrder','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Modtext;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Modtext']))
		{
			$model->attributes=$_POST['Modtext'];
			if($model->save()) {
				if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>"Text successfully added",
                        'model'=> array(
                        	'id' => $model->id,
                        	'text' => $model->text)
                        ));
                    exit;               
                }
                else {
                    $this->redirect(array('view','id'=>$model->id));
                }
			}
		}

		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial('_form', array('model'=>$model), true)));
            exit;               
        }
        else {
            $this->render('create',array('model'=>$model,));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$currentId = $id;

		if($id == 0) {
			$currentId = $_POST['id'];
		}

		$model=$this->loadModel($currentId);

		if(isset($_POST['Modtext']))
		{
			$model->attributes=$_POST['Modtext'];
			if($model->save()) {
				if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>"Text successfully edited",
                        'model'=> array(
                        	'id' => $model->id,
                        	'text' => $model->text)
                        ));
                    exit;               
                }
                else {
                    $this->redirect(array('view','id'=>$model->id));
                }
			}
		}

		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial('_form', array('model'=>$model), true)));
            exit;               
        }
        else {
            $this->render('update',array('model'=>$model,));
        }
	}

	public function actionUpdateOrder($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['order'])) {
			$newOrder = $_POST['order'];

			$model->order_elt = $newOrder;

			$model->save();

			echo CJSON::encode(array('status'=>'success', 'newOrder' => $newOrder));
		} else {
			echo CJSON::encode(array('status'=>'falure'));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$currentId = $id;

		if($id == 0) {
			$currentId = $_POST['id'];
		}

		$this->loadModel($currentId)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'success',
                'model'=> array(
                	'id' => $currentId)
                ));
            exit;               
        }
        else {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        /*
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		*/
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Modtext');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Modtext('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Modtext']))
			$model->attributes=$_GET['Modtext'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Modtext the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Modtext::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Modtext $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mod-text-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
