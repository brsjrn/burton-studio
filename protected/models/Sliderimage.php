<?php

/**
 * This is the model class for table "sliderimage".
 *
 * The followings are the available columns in table 'sliderimage':
 * @property integer $id
 * @property integer $ref_slider
 * @property string $image
 * @property integer $order_elt
 *
 * The followings are the available model relations:
 * @property Modslider $refSlider
 */
class Sliderimage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sliderimage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ref_slider, image', 'required'),
			array('ref_slider, order_elt', 'numerical', 'integerOnly'=>true),
			array('image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ref_slider, image, order_elt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'refSlider' => array(self::BELONGS_TO, 'Modslider', 'ref_slider'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ref_slider' => 'Ref Slider',
			'image' => 'Image (2200 x 1400)',
			'order_elt' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ref_slider',$this->ref_slider);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('order_elt',$this->order_elt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sliderimage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRefModel($param = array())
	{
	    $criteria = new CDbCriteria($param);
	    
	    return new CActiveDataProvider($this, array(
	            'criteria'=>$criteria,
	    ));
	}
}
