<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'isDisplay'); ?>
		<?php echo $form->textField($model,'isDisplay'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'isStared'); ?>
		<?php echo $form->textField($model,'isStared'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stared_description_text'); ?>
		<?php echo $form->textArea($model,'stared_description_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stared_order'); ?>
		<?php echo $form->textField($model,'stared_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stared_cover_image'); ?>
		<?php echo $form->textField($model,'stared_cover_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cover_image'); ?>
		<?php echo $form->textField($model,'cover_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_type'); ?>
		<?php echo $form->textField($model,'project_type',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'architect'); ?>
		<?php echo $form->textField($model,'architect',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ordre'); ?>
		<?php echo $form->textField($model,'ordre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_seo'); ?>
		<?php echo $form->textField($model,'description_seo',array('size'=>60,'maxlength'=>140)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_text'); ?>
		<?php echo $form->textArea($model,'description_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'visual_image_1'); ?>
		<?php echo $form->textField($model,'visual_image_1',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'visual_image_2'); ?>
		<?php echo $form->textField($model,'visual_image_2',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->