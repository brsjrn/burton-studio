<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Projects'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Project', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#project-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Projects</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Project/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'project-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'isDisplay',
		'isStared',
		'ordre',
		/*'stared_description_text',
		'stared_order',
		'stared_cover_image',
		'cover_image',
		'location',
		'project_type',
		'architect',
		'ordre',
		'description_seo',
		'description_text',
		'visual_image_1',
		'visual_image_2',
		'slug',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>