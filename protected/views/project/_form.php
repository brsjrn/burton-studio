<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
			<?php echo $form->error($model,'title'); ?>
			<div class="clear"></div>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'isDisplay'); ?>
			<?php echo $form->checkBox($model,'isDisplay', array('class' => 'checkHasTarget')); ?>
			<?php echo $form->error($model,'isDisplay'); ?>
			<div class="clear"></div>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'isStared'); ?>
			<?php echo $form->checkBox($model,'isStared', array('class' => 'checkHasTarget')); ?>
			<?php echo $form->error($model,'isStared'); ?>
			<div class="clear"></div>
		</div>
						
		<div class="hidden-area" id="Project_isStared_target">
			<div class="row">
				<?php echo $form->labelEx($model,'stared_description_text'); ?>
				<?php echo $form->textArea($model,'stared_description_text',array('rows'=>6, 'cols'=>50, 'class' => 'hidden-elt')); ?>
				<?php echo $form->error($model,'stared_description_text'); ?>
				<div class="clear"></div>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'stared_order'); ?>
				<?php echo $form->textField($model,'stared_order', array('class' => 'hidden-elt')); ?>
				<?php echo $form->error($model,'stared_order'); ?>
				<div class="clear"></div>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'stared_cover_image'); ?>
				<?php echo $form->fileField($model,'stared_cover_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'stared_cover_image'); ?>
			    <div class="clear"></div>
			
				<?php if(!$model->isNewRecord && $model->stared_cover_image != ''){ ?>
			        <div class='image_already_exist hidden-image-exist'>
			             <?php echo CHtml::image('../../../images/Project/' . $model->stared_cover_image,'image',array('class'=>'image_preview')); ?>
			        </div>
		        <?php } ?>
	        </div>
        </div>


		<div class="row">
			<?php echo $form->labelEx($model,'cover_image'); ?>
			<?php echo $form->fileField($model,'cover_image',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'cover_image'); ?>
			<div class="clear"></div>
		
			<?php if(!$model->isNewRecord && $model->cover_image != ''){ ?>
		        <div class='image_already_exist'>
		             <?php echo CHtml::image('../../../images/Project/' . $model->cover_image,'image',array('class'=>'image_preview')); ?>
		        </div>
	        <?php } ?>
        </div>

				<div class="row">
			<?php echo $form->labelEx($model,'location'); ?>
			<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'location'); ?>
			<div class="clear"></div>
		</div>
							<div class="row">
			<?php echo $form->labelEx($model,'project_type'); ?>
			<?php echo $form->textField($model,'project_type',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'project_type'); ?>
			<div class="clear"></div>
		</div>
							<div class="row">
			<?php echo $form->labelEx($model,'architect'); ?>
			<?php echo $form->textField($model,'architect',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'architect'); ?>
			<div class="clear"></div>
		</div>
							<div class="row">
			<?php echo $form->labelEx($model,'ordre'); ?>
			<?php 
							if($model->isNewRecord) {
		                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
		                    } else {
		                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
		                    }
						?>						<?php echo $form->error($model,'ordre'); ?>
			<div class="clear"></div>
		</div>
					
									<div class="row">
					<?php echo $form->labelEx($model,'description_text'); ?>
					<?php echo $form->textArea($model,'description_text',array('id'=>'editordescription_text')); ?>
					<?php echo $form->error($model,'description_text'); ?>
					<div class="clear"></div>
				</div>
						<div class="row">
				<?php echo $form->labelEx($model,'visual_image_1'); ?>
				<?php echo $form->fileField($model,'visual_image_1',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'visual_image_1'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->visual_image_1 != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../images/Project/' . $model->visual_image_1,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
					<div class="row">
				<?php echo $form->labelEx($model,'visual_image_2'); ?>
				<?php echo $form->fileField($model,'visual_image_2',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'visual_image_2'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->visual_image_2 != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../images/Project/' . $model->visual_image_2,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

    			<div class="row">
    				<?php echo $form->labelEx($model,'description_seo'); ?>
    				<?php echo $form->textField($model,'description_seo',array('size'=>60,'maxlength'=>140)); ?>
    				<?php echo $form->error($model,'description_seo'); ?>
    				<div class="clear"></div>
    			</div>

					<!--
					<div class="row">
						<?php echo $form->labelEx($model,'slug'); ?>
						<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'slug'); ?>
						<div class="clear"></div>
					</div>
				-->
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	
				CKEDITOR.replace( 'editorstared_description_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editordescription_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			    
</script>
