<?php
/* @var $this ProjectController */
/* @var $data Project */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isDisplay')); ?>:</b>
	<?php echo CHtml::encode($data->isDisplay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isStared')); ?>:</b>
	<?php echo CHtml::encode($data->isStared); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stared_description_text')); ?>:</b>
	<?php echo CHtml::encode($data->stared_description_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stared_order')); ?>:</b>
	<?php echo CHtml::encode($data->stared_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stared_cover_image')); ?>:</b>
	<?php echo CHtml::encode($data->stared_cover_image); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cover_image')); ?>:</b>
	<?php echo CHtml::encode($data->cover_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_type')); ?>:</b>
	<?php echo CHtml::encode($data->project_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('architect')); ?>:</b>
	<?php echo CHtml::encode($data->architect); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_seo')); ?>:</b>
	<?php echo CHtml::encode($data->description_seo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_text')); ?>:</b>
	<?php echo CHtml::encode($data->description_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visual_image_1')); ?>:</b>
	<?php echo CHtml::encode($data->visual_image_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visual_image_2')); ?>:</b>
	<?php echo CHtml::encode($data->visual_image_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />

	*/ ?>

</div>