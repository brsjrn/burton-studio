<?php
/* @var $this ModtextController */
/* @var $data Modtext */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_project')); ?>:</b>
	<?php echo CHtml::encode($data->ref_project); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_elt')); ?>:</b>
	<?php echo CHtml::encode($data->order_elt); ?>
	<br />


</div>