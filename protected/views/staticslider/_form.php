<?php
/* @var $this StaticsliderController */
/* @var $model Staticslider */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'staticslider-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row hidden_data">
						<?php echo $form->labelEx($model,'title'); ?>
						<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'title'); ?>
						<div class="clear"></div>
					</div>
										<div class="row hidden_data">
						<?php echo $form->labelEx($model,'getTitle'); ?>
						<?php echo $form->textField($model,'getTitle',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'getTitle'); ?>
						<div class="clear"></div>
					</div>
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
