<?php 
    $baseUrl = Yii::app()->baseUrl;

    if(Yii::app()->user->isGuest) {
        //$this->redirect(Yii::app()->getModule('user')->loginUrl);
        $this->redirect(array('site/login'));
    }
?>

<div id="top_admin_model">
	<h1>About content</h1>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
    
    <div id="list_statics_fields">

        <?php
        // Dialog title
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
            'id'=>'dialogTitle',
            'options'=>array(
                'title'=>'Edit title',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>550,
                'height'=>470,
            ),
        ));?> 
        <div class="divDialogForm"></div>            
        <?php $this->endWidget();?>

        <?php
        // Dialog text
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
            'id'=>'dialogText',
            'options'=>array(
                'title'=>'Edit text',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>550,
                'height'=>470,
            ),
        ));?>
        <div class="divDialogForm"></div>  
        <?php $this->endWidget();?>

        <?php
        // Dialog text
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
            'id'=>'dialogSeo',
            'options'=>array(
                'title'=>'Edit SEO',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>550,
                'height'=>470,
            ),
        ));?>
        <div class="divDialogForm"></div>  
        <?php $this->endWidget();?>

        <?php
        // Dialog image
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
            'id'=>'dialogImage',
            'options'=>array(
                'title'=>'Edit image',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>550,
                'height'=>470,
                'beforeClose'=> 'js:function() {
                    console.log("CLOOSE image");
                    $("#dialogImage div.divDialogForm").html("");
                }',
            ),
        ));?>
        <div class="divDialogForm"></div>  
        <?php $this->endWidget();?>

        <?php
        // Dialog image
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
            'id'=>'dialogVideo',
            'options'=>array(
                'title'=>'Edit video',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>550,
                'height'=>470,
                'beforeClose'=> 'js:function() {
                    console.log("CLOOSE video");
                    $("#dialogVideo div.divDialogForm").html("");
                }',
            ),
        ));?>
        <div class="divDialogForm"></div>  
        <?php $this->endWidget();?>

         <div class="admin-bloc">
            <!-- ABOUT VIDEO 1 -->
            <?php if(isset($fields['about_video_1'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_video_1']->title; ?></div>
                <div class="elt_static_value video-<?php echo $fields['about_video_1']->id; ?>">
                    <?php 
                        if(isset($fields['about_video_1']->value) && ($fields['about_video_1']->value != '')) {
                            ?>
                                <iframe width="560" height="315" src="https://player.vimeo.com/video/<?php echo $fields['about_video_1']->value ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            <?php
                        } 
                    ?>
                </div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_video_1']->id,
                        'class'=>'elt_static_video_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

        <div class="admin-bloc">
            <!-- ABOUT TITLE 1 -->
            <?php if(isset($fields['about_title_1'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_title_1']->title; ?></div>
                <div class="elt_static_value title-<?php echo $fields['about_title_1']->id; ?>">
                   <?php 
                       if(isset($fields['about_title_1']->value) && ($fields['about_title_1']->value != '')) {
                           echo $fields['about_title_1']->value;
                       } 
                   ?>
                    
                </div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_title_1']->id,
                        'class'=>'elt_static_title_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>

            <!-- ABOUT TEXT 1 -->
            <?php if(isset($fields['about_text_1'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_text_1']->title; ?></div>
                <div class="elt_static_value text-<?php echo $fields['about_text_1']->id; ?>"><?php echo $fields['about_text_1']->value; ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_text_1']->id,
                        'class'=>'elt_static_text_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

         <div class="admin-bloc">
            <!-- ABOUT IMAGE 1 -->
            <?php if(isset($fields['about_image_1'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_image_1']->title; ?> (1520 x 1160)</div>
                <div class="elt_static_value image-<?php echo $fields['about_image_1']->id; ?>"><?php
                    if(isset($fields['about_image_1']->value) && ($fields['about_image_1']->value != '')) {
                        echo '<img src="'. $baseUrl .'/images/static/'. $fields['about_image_1']->value .'" class="preview">';
                    }
                ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_image_1']->id,
                        'class'=>'elt_static_image_edit',
                        'style'=>'cursor: pointer; image-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>

            <!-- ABOUT IMAGE 2 -->
            <?php if(isset($fields['about_image_2'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_image_2']->title; ?> (1170 x 2060)</div>
                <div class="elt_static_value image-<?php echo $fields['about_image_2']->id; ?>"><?php
                    if(isset($fields['about_image_2']->value) && ($fields['about_image_2']->value != '')) {
                        echo '<img src="'. $baseUrl .'/images/static/'. $fields['about_image_2']->value .'" class="preview">';
                    }
                ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_image_2']->id,
                        'class'=>'elt_static_image_edit',
                        'style'=>'cursor: pointer; image-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

        <div class="admin-bloc">
            <!-- ABOUT TITLE 2 -->
            <?php if(isset($fields['about_title_2'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_title_2']->title; ?></div>
                <div class="elt_static_value title-<?php echo $fields['about_title_2']->id; ?>"><?php echo $fields['about_title_2']->value; ?></div>
                
                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_title_2']->id,
                        'class'=>'elt_static_title_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>

            <!-- ABOUT TEXT 2 -->
            <?php if(isset($fields['about_text_2'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_text_2']->title; ?></div>
                <div class="elt_static_value text-<?php echo $fields['about_text_2']->id; ?>"><?php echo $fields['about_text_2']->value; ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_text_2']->id,
                        'class'=>'elt_static_text_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

         <div class="admin-bloc">
            <!-- ABOUT Slider -->
            <?php if(isset($fields['about_slider_1'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_slider_1']->title; ?></div>
                <div class="elt_static_value image-<?php echo $fields['about_slider_1']->id; ?>"><?php
                    if(isset($fields['about_slider_1']->value) && ($fields['about_slider_1']->value != '')) {
                        echo '<img src="'. $baseUrl .'/images/static/'. $fields['about_slider_1']->value .'" class="preview">';
                    }
                ?></div>

                <a href="<?php echo CController::createUrl('Staticsliderimage/adminById', array('refModelId' => $fields['about_slider_1']->id)); ?>" class="elt_static_slider_edit" >Manage slider images (<?php echo $nbSliderImage; ?>)</a>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

        <div class="admin-bloc">
            <!-- ABOUT TITLE 3 -->
            <?php if(isset($fields['about_title_3'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_title_3']->title; ?></div>
                <div class="elt_static_value title-<?php echo $fields['about_title_3']->id; ?>"><?php echo $fields['about_title_3']->value; ?></div>
                
                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_title_3']->id,
                        'class'=>'elt_static_title_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>

            <!-- ABOUT TEXT 3 -->
            <?php if(isset($fields['about_text_3'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_text_3']->title; ?></div>
                <div class="elt_static_value text-<?php echo $fields['about_text_3']->id; ?>"><?php echo $fields['about_text_3']->value; ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_text_3']->id,
                        'class'=>'elt_static_text_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

         <div class="admin-bloc">
            <!-- ABOUT IMAGE 3 -->
            <?php if(isset($fields['about_image_3'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_image_3']->title; ?> (2200 x 1100)</div>
                <div class="elt_static_value image-<?php echo $fields['about_image_3']->id; ?>"><?php
                    if(isset($fields['about_image_3']->value) && ($fields['about_image_3']->value != '')) {
                        echo '<img src="'. $baseUrl .'/images/static/'. $fields['about_image_3']->value .'" class="preview">';
                    }
                ?></div>

                

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_image_3']->id,
                        'class'=>'elt_static_image_edit',
                        'style'=>'cursor: pointer; image-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>

        <div class="admin-bloc">

            <!-- ABOUT SEO -->
            <?php if(isset($fields['about_seo'])) { ?>
            <div class="elt_static">
                <div class="elt_static_label"><?php echo $fields['about_seo']->title; ?></div>
                <div class="elt_static_value seo-<?php echo $fields['about_seo']->id; ?>"><?php echo $fields['about_seo']->value; ?></div>

                <?php echo CHtml::link('Edit', "",  // the link for open the dialog
                    array(
                        'elt-id'=>$fields['about_seo']->id,
                        'class'=>'elt_static_seo_edit',
                        'style'=>'cursor: pointer; text-decoration: underline;',
                    ));
                ?>

                <div class="clear"></div>
            </div>
            <?php } ?>
        </div>
        
    </div>

    <div id="selectedEltId" class="hidden_data"></div>
</div>

<script type='text/javascript'>

    /* Static title */
    // -- Update
    function updateTitle()
    {
        <?php echo CHtml::ajax(array(
            'url'=>array('Statictitle/update/0'),
            'data'=> "js:$(this).serialize() +'&id='+ $(\"#selectedEltId\").html()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogTitle div.divDialogForm').html(data.div);
                    $('#dialogTitle div.divDialogForm form').submit(updateTitle);
                }
                else
                {
                    $('#dialogTitle div.divDialogForm').html(data.div);
                    setTimeout(\"$('#dialogTitle').dialog('close') \",1000);

                    $('.title-'+ data.model.id +'').html(data.model.value);
                }

            } ",
            ))?>;
        return false; 
    }

    // -- Update title
    $('.elt_static_title_edit').on('click', function() {
        var id = $(this).attr('elt-id');
        console.log("ID : "+ id);

        $('#selectedEltId').html(id);

        updateTitle(id);
        $('#dialogTitle').dialog('open');
    });


    /* STATIC TEXT */
    // -- Update
    function updateText()
    {
        <?php echo CHtml::ajax(array(
            'url'=>array('Statictext/update/0'),
            'data'=> "js:$(this).serialize() +'&id='+ $(\"#selectedEltId\").html()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogText div.divDialogForm').html(data.div);
                    $('#dialogText div.divDialogForm form').submit(updateText);
                }
                else
                {
                    $('#dialogText div.divDialogForm').html(data.div);
                    setTimeout(\"$('#dialogText').dialog('close') \",1000);

                    $('.text-'+ data.model.id +'').html(data.model.value);
                }

            } ",
            ))?>;
        return false; 
    }

    // -- Update text
    $('.elt_static_text_edit').on('click', function() {
        var id = $(this).attr('elt-id');
        console.log("ID : "+ id);

        $('#selectedEltId').html(id);

        updateText(id);
        $('#dialogText').dialog('open');
    });

    /* STATIC SEO */
    // -- Update
    function updateSeo()
    {
        <?php echo CHtml::ajax(array(
            'url'=>array('Staticseo/update/0'),
            'data'=> "js:$(this).serialize() +'&id='+ $(\"#selectedEltId\").html()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    console.log('fail...');
                    $('#dialogSeo div.divDialogForm').html(data.div);
                    $('#dialogSeo div.divDialogForm form').submit(updateSeo);
                }
                else
                {
                    console.log('success');
                    $('#dialogSeo div.divDialogForm').html(data.div);
                    setTimeout(\"$('#dialogSeo').dialog('close') \",1000);

                    $('.seo-'+ data.model.id +'').html(data.model.value);
                }

            } ",
            ))?>;
        return false; 
    }

    // -- Update seo
    $('.elt_static_seo_edit').on('click', function() {
        var id = $(this).attr('elt-id');
        console.log("ID : "+ id);

        $('#selectedEltId').html(id);

        updateSeo(id);
        $('#dialogSeo').dialog('open');
    });

    /* STATIC IMAGE */
    // -- Prepare udpate
    function prepareUpdateImage(id)
    {
        console.log("Prepare for id : "+ id);
        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Staticimage/update/"); ?>/'+ id,
                type: 'POST',
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogImage div.divDialogForm').html(data.div);
                        $('#dialogImage div.divDialogForm form').submit(updateImage(id));
                    }
                }
            }
        );

        return false; 
    }

    // -- Update
    function updateImage(id)
    {
        console.log("Update id : "+ id);

        var formData = new FormData($("#staticimage-form")[0])

        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Staticimage/update/"); ?>/'+ id,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogImage div.divDialogForm').html(data.div);
                        console.log("failure");
                        //$('#dialogImage div.divDialogForm form').submit(updateImage(id));
                    }
                    else
                    {
                        console.log("succes");
                        $('#dialogImage div.divDialogForm').html(data.div);
                        setTimeout(function() {
                            $('#dialogImage').dialog('close')
                        }, 1000);
                        $('.image-'+ data.model.id +'').html(data.model.value);
                    }

                }
            }
        );

        return false; 
    }

    // -- Update image
    $('.elt_static_image_edit').on('click', function() {
        var id = $(this).attr('elt-id');
        console.log("ID : "+ id);

        $('#selectedEltId').html(id);

        updateImage(id);
        $('#dialogImage').dialog('open');
    });

    /* Static video */
    // -- Update
    function updateVideo()
    {
        <?php echo CHtml::ajax(array(
            'url'=>array('Staticvideo/update/0'),
            'data'=> "js:$(this).serialize() +'&id='+ $(\"#selectedEltId\").html()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogVideo div.divDialogForm').html(data.div);
                    $('#dialogVideo div.divDialogForm form').submit(updateVideo);
                }
                else
                {
                    $('#dialogVideo div.divDialogForm').html(data.div);
                    setTimeout(\"$('#dialogVideo').dialog('close') \",1000);

                    var videoEmbeded = '<iframe width=\"560\" height=\"315\" src=\"https://player.vimeo.com/video/'+ data.model.value +'\" frameborder=\"0\" allowfullscreen></iframe>';

                    $('.video-'+ data.model.id +'').html(videoEmbeded);
                }

            } ",
            ))?>;
        return false; 
    }

    // -- Update video
    $('.elt_static_video_edit').on('click', function() {
        var id = $(this).attr('elt-id');
        console.log("ID : "+ id);

        $('#selectedEltId').html(id);

        updateVideo(id);
        $('#dialogVideo').dialog('open');
    });

</script>