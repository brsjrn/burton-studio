<?php
/* @var $this StaticsliderimageController */
/* @var $model Staticsliderimage */

$this->breadcrumbs=array(
    'About'=>array('site/aboutAdmin/'),
    'Slider images',
    //'Dates'=>array('adminById', 'refModelId'=>$evenement->id)
);


$this->menu=array(
	array('label'=>'Create slider image', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#staticsliderimage-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Images<span class="back_admin"><?php echo CHtml::link('retour', array('site/aboutAdmin')); ?></span></h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Staticsliderimage/create', 'refModelId'=>$parent->id), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'staticsliderimage-grid',
	'dataProvider'=>$model->getRefModel(array('condition'=>'ref_slider='. $parent->id)),
	'filter'=>$model,
	'columns'=>array(
		array(
	        'name'=>'image',
	        'type'=>'html',
	        'value'=>'CHtml::image(Yii::app()->baseUrl . "/images/Staticsliderimages/" . $data->image,"",array("style"=>"width:auto;height:100px;"))'
	    ),
		'order_elt',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>