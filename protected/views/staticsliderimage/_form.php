<?php
/* @var $this StaticsliderimageController */
/* @var $model Staticsliderimage */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'staticsliderimage-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row hidden_row">
						<?php echo $form->labelEx($model,'ref_slider'); ?>
						<?php echo $form->textField($model,'ref_slider', array(
					        'value'=>$parent->id
						)); ?>
						<?php echo $form->error($model,'ref_slider'); ?>
						<div class="clear"></div>
					</div>
								<div class="row">
				<?php echo $form->labelEx($model,'image'); ?>
				<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../images/Staticsliderimage/' . $model->image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
							<div class="row">
						<?php echo $form->labelEx($model,'order_elt'); ?>
						<?php echo $form->textField($model,'order_elt'); ?>
						<?php echo $form->error($model,'order_elt'); ?>
						<div class="clear"></div>
					</div>
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
