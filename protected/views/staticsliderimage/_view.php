<?php
/* @var $this StaticsliderimageController */
/* @var $data Staticsliderimage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_slider')); ?>:</b>
	<?php echo CHtml::encode($data->ref_slider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_elt')); ?>:</b>
	<?php echo CHtml::encode($data->order_elt); ?>
	<br />


</div>