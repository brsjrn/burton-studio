<?php
/* @var $this ModspaceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modspaces',
);
?>

<div id="top_admin_model">
	<h1>Modspaces</h1>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</div>
