<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'adress_line_1'); ?>
						<?php echo $form->textField($model,'adress_line_1',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'adress_line_1'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'adress_line_2'); ?>
						<?php echo $form->textField($model,'adress_line_2',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'adress_line_2'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'adress_country'); ?>
						<?php echo $form->textField($model,'adress_country',array('size'=>31,'maxlength'=>31)); ?>
						<?php echo $form->error($model,'adress_country'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'phone'); ?>
						<?php echo $form->textField($model,'phone',array('size'=>31,'maxlength'=>31)); ?>
						<?php echo $form->error($model,'phone'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'email'); ?>
						<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'email'); ?>
						<div class="clear"></div>
					</div>
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
