<?php
/* @var $this ModtitleController */
/* @var $model Modtitle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modlargetitle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'ref_project'); ?>
		<?php echo $form->textField($model,'ref_project'); ?>
		<?php echo $form->error($model,'ref_project'); ?>
	</div>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'order_elt'); ?>
		<?php echo $form->textField($model,'order_elt'); ?>
		<?php echo $form->error($model,'order_elt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->