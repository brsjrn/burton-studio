<?php
/* @var $this ModlargetitleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modlargetitles',
);
?>

<div id="top_admin_model">
	<h1>Modlargetitles</h1>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</div>
