<?php
/* @var $this ModlargetitleController */
/* @var $model Modlargetitle */

$this->breadcrumbs=array(
	'Modlargetitles'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Modlargetitle', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#modlargetitle-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Modlargetitles</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Modlargetitle/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'modlargetitle-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'ref_project',
		'order_elt',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>