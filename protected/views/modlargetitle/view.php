<?php
/* @var $this ModlargetitleController */
/* @var $model Modlargetitle */

$this->breadcrumbs=array(
	'Modlargetitles'=>array('admin'),
	$model->title,
);
?>

<div id="top_admin_model">
	<h1>Modlargetitle #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Modlargetitle/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Modlargetitle/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Modlargetitle/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">title</div><div class="view_attribute_value"><?php echo isset($model->title) ? $model->title : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ref_project</div><div class="view_attribute_value"><?php echo isset($model->ref_project) ? $model->ref_project : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">order_elt</div><div class="view_attribute_value"><?php echo isset($model->order_elt) ? $model->order_elt : "-"; ?></div><div class="clear"></div></div></div>