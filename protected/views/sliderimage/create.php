<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

$this->breadcrumbs=array(
    'Project'=>array('project/update/'. $parent->ref_project),
	'Slider image'=>array('adminById', 'refModelId'=>$parent->id),
	'Add',
);

$this->menu=array(
	array('label'=>'Manage slider images', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Add image<span class="back_admin"><?php echo CHtml::link('retour', array('Sliderimage/adminById', 'refModelId'=>$parent->id)); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'parent'=>$parent)); ?></div>