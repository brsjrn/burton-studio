<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

$this->breadcrumbs=array(
    'Project'=>array('project/update/'. $parent->ref_project),
	'Slider image'=>array('adminById', 'refModelId'=>$parent->id),
	'Update',
);
?>

<div id="top_admin_model">
	<h1>Update slider image #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('retour', array('Sliderimage/adminById', 'refModelId'=>$parent->id)); ?></span></h1>
	
	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">save</div>
		<?php echo CHtml::link('', array('Sliderimage/view', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-eye-open')); ?>
        <?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>
        <?php echo CHtml::link('', array('Sliderimage/create', 'refModelId'=>$parent->id), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>
        <div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array(
	'model'=>$model,
	'parent'=>$parent
)); ?></div>