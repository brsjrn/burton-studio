<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

$this->breadcrumbs=array(
    'Project'=>array('project/update/'. $parent->ref_project),
    'Slider image',
    //'Dates'=>array('adminById', 'refModelId'=>$evenement->id)
);


$this->menu=array(
	array('label'=>'Create Sliderimage', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sliderimage-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Images<span class="back_admin"><?php echo CHtml::link('retour', array('project/update/'. $parent->ref_project)); ?></span></h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Sliderimage/create', 'refModelId'=>$parent->id), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sliderimage-grid',
	'dataProvider'=>$model->getRefModel(array('condition'=>'ref_slider='. $parent->id)),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'image',
            'type'=>'html',
            'value'=>'CHtml::image(Yii::app()->baseUrl . "/images/Sliderimage/" . $data->image,"",array("style"=>"width:auto;height:100px;"))'
        ),

		'order_elt',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>